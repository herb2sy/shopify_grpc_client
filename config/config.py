import os


class Config(object):
    TIMEZONE = 'UTC'
    BASE_DIR = os.path.dirname(os.path.dirname(__file__))
