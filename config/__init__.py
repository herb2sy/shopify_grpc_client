
import sys
import os
sys.path.append('.')


def load_config():
    """
    Load a config class
    """
    mode = os.environ.get('PROFILES', 'local')
    print("environ mode", mode)
    try:
        if mode == 'local':
            from .local_config import LocalConfig
            return LocalConfig
        else:
            from .local_config import LocalConfig
            return LocalConfig
    except ImportError:
        from .config import Config
        return Config


CONFIG = load_config()