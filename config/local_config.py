import sys

sys.path.append('.')
from config.config import Config
import logging


# ====================
# dev environment config
# ===============================

class LocalConfig(Config):
    DEBUG = True
    HOST = '0.0.0.0'
    PORT = 5001
    API_KEY = "aa9d8cd819a12b84dc405e5aaab66d01"
    API_SECRET = "aa38e2a5f9077c6912785f1dc65d0ed7"
    API_VERSION = "2021-10"
    REDIRECT_URI = "https://app.herblee.cn/callback"
    SCOPES = ['read_checkouts', 'write_checkouts', 'read_draft_orders', 'write_draft_orders', 'read_orders',
              'write_orders',
              'read_discounts', 'write_discounts', 'read_gift_cards', 'write_gift_cards', 'read_customers',
              'write_customers',
              'read_products', 'write_products', 'read_product_listings', "read_price_rules"]
