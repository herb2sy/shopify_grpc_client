import binascii
import os


def get_state():
    return binascii.b2a_hex(os.urandom(15)).decode("utf-8")
