import shopify
import sys
from config import CONFIG

# shop_url = "dev820.myshopify.com"

# access_token = "shpua_95838a200c513a38768394d895b1f55a"

"""
订单、弃单、草稿订单权限：
read_checkouts、write_checkouts、read_draft_orders、  write_draft_orders、read_orders、write_orders
折扣、礼品卡权限：
read_discounts、write_discounts、read_gift_cards、write_gift_cards
客户信息权限：
read_customers、write_customers    
产品信息权限：
read_products、write_products、read_product_listings

"""


class Shopify:

    def __init__(self):
        shopify.Session.setup(api_key=CONFIG.API_KEY, secret=CONFIG.API_SECRET)

    @staticmethod
    def _log(content):
        print(content)

    def get_bind_shop_url(self, state: str, shop_url: str):
        """获取绑定店铺URL"""
        self._log(sys._getframe().f_code.co_name)
        new_session = shopify.Session(shop_url, CONFIG.API_VERSION)
        auth_url = new_session.create_permission_url(CONFIG.SCOPES, CONFIG.REDIRECT_URI, state)
        return auth_url

    def get_access_token(self, shop_url: str, request_params: dict):
        self._log(sys._getframe().f_code.co_name)
        session = shopify.Session(shop_url, CONFIG.API_VERSION)
        access_token = session.request_token(request_params)  # request_token will validate hmac and timing attacks
        return access_token

    def get_product(self, shop_url: str, access_token: str):
        self._log(sys._getframe().f_code.co_name)
        with shopify.Session.temp(shop_url, CONFIG.API_VERSION, access_token):
            product = shopify.Product.find()
            print(product)


if __name__ == '__main__':
    # bind_shop()
    # set_session()
    sp = Shopify()
    sp.get_product(shop_url="dev820.myshopify.com", access_token="shpua_95838a200c513a38768394d895b1f55a")
