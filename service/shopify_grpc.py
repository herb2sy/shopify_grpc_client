from models import shopify_pb2
from resource import shopify_pb2_grpc
import grpc
from concurrent import futures
from config import CONFIG
from service.shopify_service import Shopify as spb

sp = spb()


class Shopify(shopify_pb2_grpc.ShopifyServicer):

    def GetBindShopUrl(self, request, context):
        task_id = request.task_id
        state = request.state
        shop_url = request.shop_url

        url = sp.get_bind_shop_url(state, shop_url)

        return shopify_pb2.BindShopUrlRes(
            code=0,
            message="success",
            url=url
        )


def server():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    # register server
    shopify_pb2_grpc.add_ShopifyServicer_to_server(Shopify(), server)
    # start
    server.add_insecure_port('{}:{}'.format(CONFIG.HOST, CONFIG.PORT))
    print("start service user...")
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    server()
