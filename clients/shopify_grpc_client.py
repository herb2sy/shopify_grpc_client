from models import shopify_pb2
from resource import shopify_pb2_grpc
import grpc
from utils.utils import get_state
from config import CONFIG


def send_bind_shop_url():
    with grpc.insecure_channel("{}:{}".format(CONFIG.HOST, CONFIG.PORT)) as ch:
        stub = shopify_pb2_grpc.ShopifyStub(ch)
        rsp: shopify_pb2.BindShopUrlRes = stub.GetBindShopUrl(shopify_pb2.BindShopUrlReq(
            task_id="woshishei",
            state=get_state(),
            shop_url="dev820.myshopify.com"
        ))
        print(rsp.url)


if __name__ == '__main__':
    send_bind_shop_url()
